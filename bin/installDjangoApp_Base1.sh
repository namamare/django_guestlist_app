#!/bin/bash

projectName="djangoAppsProject"
appName="guestList"

echo DOCKER BUILD ----------------#  
#docker-compose build #--no-cache

echo START PROJECT ---------------# 
python3 -V
python3 -m django --version

#echo CREATE PROJECT -------------#
#python3 /home/namm/.local/bin/django-admin startproject $projectName

echo COPY DOCKER to $projectName ---------#
cp requirements.txt $projectName
cp dockerfile $projectName
cp docker-compose.yml $projectName

#echo MOVE INTO PROJECT ----------# 
#cd $projectName/

#echo MANAGE MIGRATE -----------#
#python3 manage.py startapp $appName
#echo MANAGE MIGRATE -----------#
#python3 manage.py migrate

#echo cd
#cd $appName/

#mkdir templates
#mkdir static
#mkdir templates/$appName
#mkdir static/img
#mkdir static/img/$appName
#mkdir static/css
#mkdir static/css/$appName

echo ../.. ------------#
cd ../..
echo git pull reset
git clone https://namamare@bitbucket.org/namamare/django_guestlist_app.git
git pull
git reset --hard
git status
git branch


echo CH $USER:$USER 777 .
chown -R $USER:$USER .
chmod -R 777 .

echo cd
cd $projectName/

echo DOCKER UP ----------# 
docker-compose up


#docker-compose up

# START DJANGO WEB SERVER
# python manage.py runserver 8080

echo START APP ----------#  
#ls -la
#python -m django --version
#

#echo "##### > (dirname "")/"
#cd "$(dirname "$rootDir")"
#ls -la



#echo "##### > ch" 
#chown -R $USER:$USER .
#chmod -R 777 .
#ls -la


#echo "compose up" 
#docker-compose up
