#!/bin/bash
set -e
projectName="django_guestlist_app"
appName="guestList"

echo DOCKER BUILD ----------------#  
docker-compose build #--no-cache

echo START PROJECT ---------------# 
python3 -V
python3 -m django --version
git --version
echo GIT INIT ------------#
git init 

echo GIT CONFIG CRED ----------#
git config --global credential.helper cache
git config credential.helper 'cache --timeout=3600'

echo GIT CLONE ------------#
git clone https://namamare@bitbucket.org/namamare/django_guestlist_app.git

#echo USER ADD TO ROOT ----------#
#usermod -a -G namm root

echo CH namm:namm 777 .  -----------#
chown -R namm:namm .
chmod -R 777 .

#echo GIT FETCH ------------#
#git fetch https://namamare@bitbucket.org/namamare/django_guestlist_app.git

echo ./cd $projectName -----------#
cd $projectName

echo GIT SET UPSTREAM ------------#
git branch --set-upstream-to=origin master
#git branch --track FETCH_HEAD
#git status
#echo GIT PULL ---------------#
#git pull
echo GIT DISCARD -- ------------#
git checkout --

echo COPY DOCKER to $projectName -----------#
#cp requirements.txt $projectName
#cp dockerfile $projectName
#cp docker-compose.yml $projectName

echo COPY ISNTALL SCRIPTS ----------#
#mkdir bin
cp ../*.sh bin



echo MANAGE MIGRATE -----------#
python manage.py startapp $appName
echo MANAGE MIGRATE -----------#
python manage.py migrate

echo CH namm:namm 777 .
chown -R namm:namm .
chmod -R 777 .


echo DOCKER UP ----------# 
docker-compose up



asd



