from django.apps import AppConfig
# from django.contrib.admin.apps import AdminConfig

class GuestListConfig(AppConfig):
    name = 'guestlist'
    verbose_name = "Guest List App"
    label = 'guestlist'

