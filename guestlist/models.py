# from django.utils import timezone
from django.db import models

# from django.contrib.auth.models import User
# from django.conf import settings
# from enum import Enum
# from django.contrib.contenttypes.models import ContentType
import uuid
from django.contrib.auth.models import AbstractUser


class CustomUser(AbstractUser):

    decoy = models.CharField(max_length=2)

    class Meta:
        verbose_name = "user"
        verbose_name_plural = "Users"


class Host(CustomUser):
    class Meta:
        verbose_name = "host"
        verbose_name_plural = "Hosts"


class Location(models.Model):
    countryCode = models.CharField(max_length=2)
    zip1 = models.CharField(max_length=4)
    zip2 = models.CharField(max_length=3)
    city = models.CharField(max_length=16)
    street = models.CharField(max_length=64)
    number = models.CharField(max_length=4)
    info = models.CharField(max_length=52)
    latitude = models.CharField(max_length=8)
    longitude = models.CharField(max_length=8)
    phone = models.CharField(max_length=12)
    prefix = models.CharField(max_length=6)

    class Meta:
        verbose_name = "location"
        verbose_name_plural = "Locations"


class Categories(models.Model):
    cat1 = models.CharField(max_length=16)
    cat2 = models.CharField(max_length=16)
    cat3 = models.CharField(max_length=16)
    cat4 = models.CharField(max_length=16)
    cat5 = models.CharField(max_length=16)

    class Meta:
        verbose_name = "categories"
        verbose_name_plural = "Category"
        unique_together = ("cat1", "cat2", "cat3", "cat4", "cat5")


class Table(models.Model):
    name = models.CharField(max_length=32)
    guestName = models.CharField(max_length=32)
    price = models.PositiveSmallIntegerField()
    maxNoPeople = models.PositiveSmallIntegerField()

    class Meta:
        verbose_name = "table"
        verbose_name_plural = "Tables"


class PublicRelations(CustomUser):
    class Meta:
        verbose_name = "public_relations"
        verbose_name_plural = "PublicRelations"


class Event(models.Model):
    uid = models.UUIDField(
        unique=True,
        editable=False,
        default=uuid.uuid4,
        verbose_name="Public identifier",
    )
    name = models.CharField(max_length=16)
    email = models.CharField(max_length=64)
    location = models.PositiveSmallIntegerField()
    language = models.CharField(max_length=2)
    created = models.DateTimeField(auto_now_add=True)
    start_time = models.DateTimeField(auto_now=True)
    event_url = models.CharField(max_length=1024)
    end_time = models.DateTimeField(auto_now=True)

    # Foreign keys
    nr_mesas = location = models.PositiveSmallIntegerField()
    location = models.ManyToManyField(Location)
    categories = models.ManyToManyField(Categories)
    host = models.OneToOneField(Host, on_delete=models.PROTECT, blank=True, null=True)
    tables = models.ManyToManyField(Table)

    class Meta:
        verbose_name = "event"
        verbose_name_plural = "Events"


class Guest(CustomUser):
    gender = models.CharField(max_length=1)
    gotIn = models.DateTimeField(default=None)

    class Meta:
        verbose_name = "guest"
        verbose_name_plural = "Guests"


class Invite(models.Model):
    pr = models.OneToOneField(
        PublicRelations, on_delete=models.PROTECT, blank=True, null=True
    )
    guest = models.OneToOneField(Guest, on_delete=models.PROTECT, blank=True, null=True)
    event = models.OneToOneField(Event, on_delete=models.PROTECT, blank=True, null=True)

    class Meta:
        unique_together = ("guest", "event")
