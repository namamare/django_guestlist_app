from django.urls import path
from . import views

urlpatterns = [
    # path('', views.index , name='index'),
    path("", views.home, name="guestlist-home"),
    path("about/", views.about, name="guestlist-about"),
    path("login/", views.about, name="guestlist-login"),
] 

# ex: /guestlist/
# path('', views.index, name='index'),
# ex: /guestlist/5/
# path('<int:question_id>/', views.manager_list, name='detail'),
# ex: /guestlist/5/results/
# path('<int:question_id>/results/', views.results, name='results'),
# ex: /guestlist/5/vote/
# path('<int:question_id>/vote/', views.vote, name='vote'),s

