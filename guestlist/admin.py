from django.contrib import admin

# Register your models here.
from .models import Host, Event
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser
from django.contrib.auth.admin import UserAdmin

# class AdminSiteConfig(admin.AdminSite):
#     verbose_name = "Guest List Admin"
#     name = "guest_list_admin"
#     label = "guest_list_admin"


class CustomUserAdmin(UserAdmin):
    # verbose_name = "Guest List Admin"
    # name = "guest_list_admin"
    # label = "guest_list_admin"

    add_form = CustomUserCreationForm
    form = CustomUserChangeForm


admin.site.register(CustomUser, CustomUserAdmin)


# class CustomHostAdmin(UserAdmin):
#     verbose_name = "Guest List Host"
#     name = "guest_list_host"
#     label = "guest_list_host"
#     model = Host
#     list_display = ["email", "username"]


# admin.site.register(Host, CustomHostAdmin)

