

// # EditorConfig is awesome: https://EditorConfig.org

// # top-most EditorConfig file
// root = true
// function undefined() {
//     alert('start');
//   }
// # Unix-style newlines with a newline ending every file
// [*]
// end_of_line = lf
// insert_final_newline = true

// # Matches multiple files with brace expansion notation
// # Set default charset
// [*.{js,py}]
// charset = utf-8

// # 4 space indentation
// [*.py]
// indent_style = space
// indent_size = 4

// # Tab indentation (no size specified)
// [Makefile]
// indent_style = tab

// # Indentation override for all JS under lib directory
// [js/**.js]
// indent_style = space
// indent_size = 2

// # Matches the exact files either package.json or .travis.yml
// [{package.json,.travis.yml}]
// indent_style = space
// indent_size = 2

window.addEventListener('load', function () {
    console.log('This function is executed once the page is fully loaded');

    // alert('alive');
    var scrollY = function updateScrollY() { window.scrollY; }





    window.addEventListener("scroll", function (evt) {
        // console.log(window.scrollY);
        //console.log(document.getElementsByClassName("bottom-header")[0].classList);
        var element = document.getElementById("bottom-header");
        var element2 = document.getElementById("main");
        if (window.scrollY > 430) {
//      if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
            // console.log("bigger");
            element.classList.add("halfway-scrolling-header");
            element2.classList.add("halfway-scrolling-header-main");
        }
        
        // halfway-scrolling-header-main

        if (window.scrollY < 430) {
            // console.log("smaller");
            element.classList.remove("halfway-scrolling-header");
            element2.classList.remove("halfway-scrolling-header-main");
            // element[0].classList.add("halfway-scrolling-header");

        }
    });
});