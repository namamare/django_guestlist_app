from django.shortcuts import render
from django.http import HttpResponse

from guestlist.models import Host

# event = [
#     {
#         "author": "nuno",
#         "title": "my first post",
#         "content": "fitst post text and cenas",
#         "date_posted": "August 9, 2018",
#     },
#     {
#         "author": "JANE",
#         "title": "my first post from JANE",
#         "content": "fitst post text and cenas JANE",
#         "date_posted": "August  10, 2018",
#     },
# ]


def home(request):
    # context = {"event": event}
    return render(request, "guestlist/home.html", {"title": "Home"})


def about(request):

    return render(request, "guestlist/about.html", {"title": "About"})


def register(request):

    return render(request, "guestlist/register.html", {"title": "Register"})


# def managerList(request):

#     return render(
#         request, "guestlist/host_list.html", {"manager_list": Host.objects.all()}
#     )




# def createEvent(request):
#     return render(request, "guestList/login.html")

