from django.urls import path
from users import views as user_views

urlpatterns = [
    # path('', views.index , name='index'),
    # path('', views.home , name='guest-list-home'),

    # path(" ", user_views.register, name="register"),
    # path('register/', user_views.register, name="register"),
    # path('login/', user_views.login, name="login"),
    # path('logout/', user_views.logout, name="logout"),
]


# ex: /guestlist/
# path('', views.index, name='index'),
# ex: /guestlist/5/
# path('<int:question_id>/', views.manager_list, name='detail'),
# ex: /guestlist/5/results/
# path('<int:question_id>/results/', views.results, name='results'),
# ex: /guestlist/5/vote/
# path('<int:question_id>/vote/', views.vote, name='vote'),s

